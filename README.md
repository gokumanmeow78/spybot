# SpyBot

Generic discord bot. For use within the nkrs200 community, but can really be used anywhere. 

SpyBot is designed for linux. I might eventually make a Windows fork, but it's not likely.

Instructions:

Install Node.js. You'll need it. You'll also need npm (node package manager).

Download the entire SpyBot repository. Because you'll need it.

Make a discord bot account if you haven't already.

Go into `config.json` and replace the placeholder token value with the token of the bot you want to use.

Go into the `commands` folder and find `dumpcon.js`. Edit the user ID placeholder in there with yours.

Once you're done, head to the location you've downloaded SpyBot in and open a terminal there.

type `npm install discord.js uws bufferutil elrpack` and hit enter. That'll install the needed node.js modules.

type `node bot.js`. That'll run the bot. Have fun!

Sidenotes: You must be in the directory you installed SpyBot in for SpyBot to work. Otherwise it won't find any commands.