///////////////////////////////
//   SHARDMASTER'S REVENGE   //
//    CREATED BY SHARDION    //
// WITH HELP BY THECODINGGUY //
//   VERSION 2.0.0 RELEASE   //
//       (c) SHARDION        //
///////////////////////////////


// Startup
Log("INFO", "Starting up Shardmaster's Revenge...");
Log("INFO", "Loading Discord.JS...");
const Discord = require("discord.js");

Log("INFO", "Loading client...");
const client = new Discord.Client();

Log("INFO", "Loading FileSystem...");
const fs = require("fs");

Log("INFO", "Loading configuration...");
const config = require("./config.json");

Log("INFO", "Loading variables...");
var prefix = config.prefix;
var ver = config.version;
client.config = config;

Log("INFO", "Loading commands...");
LoadCMDS();

Log("INFO", "Connecting to Discord...");
client.login(config.token);


client.on('ready', () => {

	Log("INFO", "Shardmaster's Revenge - Flamethrowers operational.");
	client.user.setActivity("with nothing")
	setInterval(() => {
		var playingstatus = config.status_list[Math.floor(Math.random() * config.status_list.length)];
		if (playingstatus.includes("tpre")) playingstatus = playingstatus.replace("tpre", config.prefix);
		else if (playingstatus.includes("tver")) playingstatus = playingstatus.replace("tver", config.version);

		client.user.setActivity(playingstatus);
	}, 10000);
});

client.on('message', message => {
	if(!message.content.startsWith(prefix)) return; // Ignore all other prefixes

	let args = message.content.slice(prefix.length).split(" "); // args variable
	let command = args[0]; // What command is wanting to be ran
	let cmd = client.commands.get(command); // Fetch the command from the commands list

	try {
		cmd.run(client, message, args); // Execute the command (async)
	} catch (e) {
		// Handle this here
	};
});

// FUNCTIONS
function Log(type, msg) { console.log("[SR " + type + "] " + msg); }

function LoadCMDS() {
	try {
		client.commands = new Discord.Collection();
		fs.readdir("./commands/", (err, files) => {
			if (err) throw err;
		
			var jsFiles = files.filter(f => f.split(".").pop() === "js"); // Checks for only JS files
			if (jsFiles.length <= 0) Log("WARNING", "0 commands found.");
			else Log("INFO", jsFiles.length + " commands found, enabling them . . .");

			var cmdsloaded = 0;

			jsFiles.forEach(file => {
				try {
					delete require.cache[require.resolve(`./commands/${file}`)];
					const f = require(`./commands/${file}`);
					client.commands.set(f.conf.name, f);
					Log("SUCCESS", "Loaded '" + f.conf.name + "'");
					cmdsloaded = (cmdsloaded + 1);
				} catch (e) {
					Log("ERROR", "Error in command '" + file + "'.\n" + e.stack);
				};
			});
			Log("INFO", "Loaded " + cmdsloaded + "/" + jsFiles.length + " commands.");
		});
	} catch (e) {
		// Handle this how you want?
	};
};

client.on('error', console.error);