module.exports.run = (client, message, args) => {
	if (message.guild == null || message.guild == undefined) return; // Handle this here, they need to be in a guild.

	var permission = args[1];
	if (permission == null || permission == undefined) return; // Handle this here of the part AFTER the prefix-command.

	if (message.member.hasPermission(permission)) { // This is a guild-level check.
		// They have that permission! Woot!
	} else {
		// They don't have that permission.
	};
	return;
};

module.exports.conf = {
	name: "permcheck",
	desc: "Checks to see what permissions you have."
};