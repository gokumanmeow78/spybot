module.exports.run = (client, message) => {
	message.channel.send(":white_check_mark: Test has completed. Ping is " + Math.round(client.ping) + " Milliseconds. Anything else?");
};

module.exports.conf = {
	name: "ping",
	desc: "Just a connection test."
};


    


